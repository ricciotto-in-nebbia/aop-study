package com.riccio;

import com.riccio.service.ApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@RequiredArgsConstructor
@PropertySource("application-local.properties")
@SpringBootApplication
public class AspectStudyApplication implements CommandLineRunner {

  private final ApplicationService applicationService;

  public static void main(String[] args) {
    SpringApplication.run(AspectStudyApplication.class, args);
  }

  public void run(String... args) {
    applicationService.doWork();
  }
}
