//package com.riccio.config;
//
//import java.io.File;
//import java.net.URL;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.util.Arrays;
//import java.util.List;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.core.io.Resource;
//
//@Configuration
//public class ProfileScannerConfiguration {
//
//  public static final String YML = ".yml";
//  public static final String YAML = ".yaml";
//  public static final String PROPERTIES = ".properties";
//
//  @Bean
//  public PropertySourcesPlaceholderConfigurer loadProperties() {
//    PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
//    YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
//    yaml.setResources(findClassPathResources());
//    configurer.setProperties(yaml.getObject());
//    return configurer;
//  }
//
//  private Resource[] findClassPathResources() {
//    URL url = this.getClass().getResource("/");
//    File file = new File(url.getPath());
//    String[] list = file.list();
//
//    return Arrays.stream(list)
//        .filter(this::filter)
//        .map(ClassPathResource::new).toArray(ClassPathResource[]::new);
//  }
//
//  private boolean filter(String string) {
//    return string.endsWith(YML) || string.endsWith(YAML) || string.endsWith(PROPERTIES);
//  }
//}
