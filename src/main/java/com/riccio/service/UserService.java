package com.riccio.service;

import com.riccio.model.User;
import java.util.List;

public interface UserService {

  User save(User user);

  User findUser(String name);

  List<User> findAllUsers();

  void deleteAll();
}
