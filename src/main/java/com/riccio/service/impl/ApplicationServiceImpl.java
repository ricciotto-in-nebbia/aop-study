package com.riccio.service.impl;

import com.riccio.model.User;
import com.riccio.repository.AddressRepository;
import com.riccio.service.ApplicationService;
import com.riccio.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ApplicationServiceImpl implements ApplicationService {

  private final UserService userService;
  private final AddressRepository addressRepository;

  @Value("${TEST_VALUE}")
  private String pass;

  @Override
  public void doWork() {
    System.out.println("pass: " + pass);

    addressRepository.findAll();
    addressRepository.findById(35L);

    User user = User.builder()
        .name("UserName")
        .lastName("UserLastName")
        .build();

    User savedUser = userService.save(user);
    System.out.println(savedUser);
  }
}
