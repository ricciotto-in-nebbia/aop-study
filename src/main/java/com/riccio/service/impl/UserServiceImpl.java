package com.riccio.service.impl;

import com.riccio.model.User;
import com.riccio.repository.UserRepository;
import com.riccio.service.UserService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;

  @Override
  public User save(User user) {
    return userRepository.save(user);
  }

  @Override
  public User findUser(String name) {
    return userRepository.findByName(name);
  }

  @Override
  public List<User> findAllUsers() {
    return userRepository.findAll();
  }

  @Override
  public void deleteAll() {
    userRepository.deleteAll();
  }
}
